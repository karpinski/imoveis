$(document).ready(function () {
    let numberOfSlides = $('#slider .slide').length;

    setInterval(function () {
        let currentSlide = $('#slider .slide').not('.hidden');
        const currentSlideNumber = parseInt(currentSlide.attr('id').match(/\d+/));

        currentSlide.addClass('hidden');

        if (currentSlideNumber < numberOfSlides)
        {
            $('#slider #slide-' + (currentSlideNumber + 1) + '.slide').each(function () {
                $(this).removeClass('hidden');
            });
        }
        else if (currentSlideNumber === numberOfSlides)
        {
            $('#slider #slide-1.slide').removeClass('hidden');
        }
    }, 3500);
});